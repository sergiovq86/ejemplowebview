package com.estech.ejemplowebview;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ProgressBar;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    WebView webView;
    ProgressBar progressBar;
    Button adelante;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        progressBar = findViewById(R.id.progressBar);

        webView = findViewById(R.id.webview);
        webView.setWebViewClient(new MyBrowser());
        webView.setWebChromeClient(new MyClient());

        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webView.loadUrl("https://escuelaestech.es/");

        adelante = findViewById(R.id.button);

        adelante.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                webView.goForward();
            }
        });
    }
//
//    @Override
//    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        // Comprueba si el botón pulsado es 'ATRAS'
//        if ((keyCode == KeyEvent.KEYCODE_BACK) && webView.canGoBack()) {
//            webView.goBack();
//            return true;
//        }
//        // Si no hay páginas en el historial o no es el botón ATRAS, que ejecute su funcionalidad normal
//        return super.onKeyDown(keyCode, event);
//    }

    @Override
    public void onBackPressed() {
        // si hay páginas en el historial, vamos atrás
        if (webView.canGoBack()) {
            webView.goBack();
        } else {
            // si no, pulsación atrás
            super.onBackPressed();
        }
    }

    private class MyBrowser extends WebViewClient {

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if ("escuelaestech.es".equals(Uri.parse(url).getHost())) {
                // Esta es mi web, se devuelve false y no sobreescribe el método
                return false;
            }
            // Aquí se lanzará en el navegador la nueva url
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            startActivity(intent);
            return true;
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            progressBar.setVisibility(View.VISIBLE);

        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            progressBar.setVisibility(View.GONE);
        }


    }

    private class MyClient extends WebChromeClient {
        @Override
        public void onProgressChanged(WebView view, int newProgress) {
            super.onProgressChanged(view, newProgress);
            progressBar.setProgress(newProgress);
        }
    }

}
